import Vue from 'vue'
import VueSwal from 'vue-swal'
import VueMarkdown from 'vue-markdown'

import DateFilter from '@/common/date.filter'

Vue.use(VueSwal)

Vue.component('vue-markdown', VueMarkdown)
Vue.filter('date', DateFilter)
